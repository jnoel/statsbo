# coding: utf-8

class SAP < ActiveRecord::Base
	self.abstract_class = true
	self.pluralize_table_names = false
	establish_connection(YAML.load_file('../configuration.yaml'))
				  
	# Prevent creation of new records and modification to existing records
  def readonly?
    return true
  end
 
  # Prevent objects from being destroyed
  def before_destroy
    raise ActiveRecord::ReadOnlyRecord
  end
end

class Oslp < SAP
	has_many :oinv
end

class Oinv < SAP
	belongs_to :oslp, :primary_key => "SlpCode", :foreign_key => "SlpCode" 
end

class Oitm < SAP
	has_many :oitw
end

class Oitw < SAP
	belongs_to :oitm, :primary_key => "ItemCode", :foreign_key => "ItemCode" 
end

class Opdn < SAP
	
end

class Ufd1 < SAP

end
