#!/usr/bin/env ruby
# coding: utf-8

require 'yaml'

config = 'configuration.yaml'

if File.exist?(config)
	puts "Où se trouve le serveur SAP ? [#{YAML.load_file(config)['host']}]"
	host = gets.chomp
	host = YAML.load_file(config)['host'] if host.empty?

	puts "Quel est le nom d'utilisateur de la base de données ? [#{YAML.load_file(config)['username']}]"
	user = gets.chomp
	user = YAML.load_file(config)['username'] if user.empty?

	puts "Quel est le mot de passe ? [#{YAML.load_file(config)['password']}]"
	password = gets.chomp
	password = YAML.load_file(config)['password'] if password.empty?

	puts "Quelle est la base de données ? [#{YAML.load_file(config)['database']}]"
	db = gets.chomp
	db = YAML.load_file(config)['database'] if db.empty?
else
	puts "Où se trouve le serveur SAP ?"
	host = gets.chomp

	puts "Quel est le nom d'utilisateur de la base de données ?"
	user = gets.chomp

	puts "Quel est le mot de passe ?"
	password = gets.chomp

	puts "Quelle est la base de données ?"
	db = gets.chomp
end

f = File.new(config, 'w')
f.write("adapter: sqlserver\n")
f.write("host: #{host}\n")
f.write("username: #{user}\n")
f.write("password: #{password}\n")
f.write("database: #{db}")
f.close

if RUBY_PLATFORM.include?("linux") 
	f = File.new('statSBO.sh', 'w')
	f.write("#!/bin/bash\n")
	f.write("cd #{File.join(Dir.getwd,'src')}\n")
	f.write("ruby statSBO.rbw\n")
	f.chmod(0770)
	f.close

	f = File.new(File.join(Dir.home,'.local/share/applications/statSBO.desktop'), 'w')
	f.write("#!/usr/bin/env xdg-open\n\n")
	f.write("[Desktop Entry]\n")
	f.write("Encoding=UTF-8\n")
	f.write("Name=statSBO\n")
	f.write("Name[fr]=statSBO\n")
	f.write("Name[fr_FR]=statSBO\n")
	f.write("Exec=#{File.join(Dir.getwd,'statSBO.sh')}\n")
	f.write("Icon=#{File.join(Dir.getwd,'src/resources/icon.png')}\n")
	f.write("Terminal=false\n")
	f.write("Type=Application\n")
	f.write("StartupNotify=true\n")
	f.write("Categories=Office\n")
	f.chmod(0770)
	f.close
elsif RUBY_PLATFORM.include?("mingw")
	f = File.new('statSBO.bat', 'w')
	f.write("cd #{File.join(Dir.getwd,'src')}\n")
	f.write("ruby statSBO.rbw\n")
	f.close
end

exec("bundle install")
