# coding: utf-8

class EditionFeuilleRoute < Prawn::Document
	
	def initialize results, chemin, titre
	
		super(	:page_layout => :landscape,
						:page_size => 'A4'
				 )
				 
		en_tete titre
		corps results
		rendu chemin

	end
	
	def en_tete titre
		
		bounding_box([0,cursor], :width => bounds.right) do
  		text titre, :size => 12
		end
		move_down 20
	
	end
	
	def corps results
		  		 		
		bounding_box([0,cursor], :width => bounds.right) do
  		t = table(results) do |table|
  			table.header = true
  			table.cell_style = { :size => 8, :align => :right}
  			table.row(0).style :align => :center
  			table.column(0).style :align => :left
			end
		end
	
	end
	
	def rendu chemin

		render_file chemin
	
	end
	
end
