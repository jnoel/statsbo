# coding: utf-8

class Edition < Prawn::Document
	
	attr_reader :fichier
	
	def initialize resultats, mois, chemin, datedeb, datefin, commercial
	
		super(	:page_layout => :landscape,
						:page_size => 'A4'
				 )
				 
		en_tete mois, datedeb, datefin, commercial
		corps resultats
		rendu chemin

	end
	
	def en_tete mois, datedeb, datefin, commercial
		
		bounding_box([0,cursor], :width => bounds.right) do
  		text "Résultat par #{mois ? "mois" : "semaine"} pour la période du #{datedeb.strftime("%d/%m/%Y")} au #{datefin.strftime("%d/%m/%Y")} pour le commercial #{commercial}", :size => 12
		end
		move_down 20
	
	end
	
	def corps resultats
  		 		
		bounding_box([0,cursor], :width => bounds.right) do
  		t = table(resultats) do |table|
  			table.header = true
  			table.cell_style = { :size => 8, :align => :right}
  			table.row(0).style :align => :center
  			table.column(0).style :align => :left
			end
		end
	
	end
	
	def rendu chemin

		render_file chemin
	
	end
	
end
