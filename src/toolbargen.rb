# coding: utf-8

class ToolBarGen_box < Gtk::Toolbar

	def initialize window
	
		super()
		
    @window = window
    
    set_toolbar_style Gtk::Toolbar::Style::BOTH
    
    taille = 48
		
		commercial_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/commerciaux.png" ), :label => "Commerciaux" )
		articles_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/articles.png" ), :label => "Articles" )
		cadencier_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/cadencier.png" ), :label => "Cadencier" )
		feuille_route_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/feuille_route.png" ), :label => "Feuille de route" )
    
    abouttb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/about.png" ), :label => "A Propos" )
    quittb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/exit.png" ), :label => "Quitter" )
	
		commercial_tb.signal_connect( "clicked" ) {
			window.affiche window.stat_commercial
		}
		
		articles_tb.signal_connect( "clicked" ) {
			window.affiche window.stat_articles
			window.stat_articles.focus
		}
		
		cadencier_tb.signal_connect( "clicked" ) {
			window.affiche window.cadencier
			window.cadencier.focus
		}
		
		feuille_route_tb.signal_connect( "clicked" ) {
			window.affiche window.feuille_route
			window.feuille_route.focus
		}	
	
		abouttb.signal_connect( "clicked" ) { 
			about = About.new window.version
			about.signal_connect('response') { about.destroy }
		}
		  
	  quittb.signal_connect( "clicked" ) { 
	  	window.quit
	  }
        
    tool = [commercial_tb, articles_tb, cadencier_tb, feuille_route_tb, Gtk::SeparatorToolItem.new, abouttb, quittb]
        
    tool.each_index { |i| 
    	self.insert tool[i], i
    }
	
	end

end
