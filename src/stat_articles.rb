# coding: utf-8

class StatArticles < Gtk::Box

	def initialize window
	
		super(:vertical, 2)
		
		@window = window
		
		self.pack_start formulaire, :expand => false, :fill => false, :padding => 2
		self.pack_start tableau, :expand => true, :fill => true, :padding => 2
		
		hbox = Gtk::Box.new :horizontal, 2
		# Bouton PDF
		@pdf = Gtk::Button.new
		hboxvalider = Gtk::Box.new :horizontal, 2
		hboxvalider.add Gtk::Image.new( :file => "./resources/pdf.png" )
		hboxvalider.add Gtk::Label.new "Edition PDF"
		@pdf.add hboxvalider
		hbox.add @pdf
		@pdf.sensitive = false
		@pdf.signal_connect( "clicked" ) { pdf }
		
		# Bouton Calc
		@calc = Gtk::Button.new
		hboxcalc = Gtk::Box.new :horizontal, 2
		hboxcalc.add Gtk::Image.new( :file => "./resources/calc.png" )
		hboxcalc.add Gtk::Label.new "Export Tableur"
		@calc.add hboxcalc
		hbox.add @calc
		@calc.sensitive = false
		@calc.signal_connect( "clicked" ) { export_tableur }
		
		align = Gtk::Alignment.new 1, 0.5, 0, 0
		align.add hbox
		
		self.pack_end align, :expand => false, :fill => false, :padding => 2
	
	end
	
	def formulaire
		
		hbox = Gtk::Box.new :horizontal, 2
		@calcule = Gtk::Button.new
		hboxcalcule = Gtk::Box.new :horizontal, 2
		hboxcalcule.add Gtk::Image.new( :file => "./resources/validate.png" )
		hboxcalcule.add Gtk::Label.new "Calculer"
		@calcule.add hboxcalcule
		hbox.pack_start @calcule, :expand => false, :fill => false, :padding => 2
		
		@calcule.signal_connect("clicked") {refresh}
		
		return hbox
		
	end
	
	def tableau
	
		# list_store (article, cad, code, groupe, colisage, p_cpt, p_credit, p_ha, p_revient, stock)
		@list_store = Gtk::TreeStore.new(String, String, String, String, String, String, String, String, String, String)
		@view = Gtk::TreeView.new(@list_store)
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		col = Gtk::TreeViewColumn.new("Article", renderer_left, :text => 0)
		col.sort_column_id = 0
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("CAD", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("N°Article", renderer_left, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Code", renderer_left, :text => 3)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Groupe", renderer_left, :text => 4)
		col.sort_column_id = 4
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Prix Comptant", renderer_right, :text => 5)
		col.sort_column_id = 5
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Prix Crédit", renderer_right, :text => 6)
		col.sort_column_id = 6
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Prix HA", renderer_right, :text => 7)
		col.sort_column_id = 7
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Prix Revient", renderer_right, :text => 8)
		col.sort_column_id = 8
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Stock", renderer_right, :text => 9)
		col.sort_column_id = 9
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic,:automatic)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def pdf
		
		tab = []
		
		@articles.each do |a|
			tab << [a.nom, a.code_barre, a.code, a.groupe, a.colisage, a.prix_comptant, a.prix_credit, a.prix_achat, a.prix_revient, (a.stock ? a.stock : 0.0)]
		end

		if !tab.empty?
			imprimer tab
		else
			@window.message_info "Aucun résultat pour cette sélection."
		end
	
	end
	
	def requete
	
		# On sélectionne les données
		req = "
SELECT SUB.ItmsGrpCod AS groupe, SUB.ItemCode AS code, SUB.ItemName AS nom, SUB.CardCode AS fournisseur, SUB.SalFactor2 AS colisage, SUB.CodeBars AS code_barre, MAX(OnHand) as stock, MAX(SUB.PCpt) AS prix_comptant, MAX(SUB.PCrd) AS prix_credit, MAX(SUB.PA) AS prix_achat, MAX(SUB.PR) AS prix_revient
FROM

(
SELECT T0.ItmsGrpCod, T0.ItemCode, T0.ItemName, T0.CardCode, T0.SalFactor2, T0.CodeBars, T2.Onhand, T1.Price AS PCpt, 0 AS PCrd, 0 AS PA, 0 AS PR
FROM OITM T0 INNER JOIN ITM1 T1 ON T0.ItemCode=T1.ItemCode
LEFT JOIN OITW T2 ON T0.ItemCode = T2.ItemCode
WHERE T1.PriceList=1 AND T2.WhsCode='01'

UNION ALL

SELECT T0.ItmsGrpCod, T0.ItemCode, T0.ItemName, T0.CardCode, T0.SalFactor2, T0.CodeBars, T2.Onhand, 0 AS PCpt, T1.Price AS PCrd, 0 AS PA, 0 AS PR
FROM OITM T0 INNER JOIN ITM1 T1 ON T0.ItemCode=T1.ItemCode
LEFT JOIN OITW T2 ON T0.ItemCode = T2.ItemCode
WHERE T1.PriceList=2 AND T2.WhsCode='01'

UNION ALL

SELECT T0.ItmsGrpCod, T0.ItemCode, T0.ItemName, T0.CardCode, T0.SalFactor2, T0.CodeBars, T2.Onhand, 0 AS PCpt, 0 AS PCrd, T1.Price AS PA, 0 AS PR
FROM OITM T0 INNER JOIN ITM1 T1 ON T0.ItemCode=T1.ItemCode
LEFT JOIN OITW T2 ON T0.ItemCode = T2.ItemCode
WHERE T1.PriceList=3 AND T2.WhsCode='01'

UNION ALL

SELECT T0.ItmsGrpCod, T0.ItemCode, T0.ItemName, T0.CardCode, T0.SalFactor2, T0.CodeBars, T2.Onhand, 0 AS PCpt, 0 AS PCrd, 0 AS PA, T1.Price AS PR
FROM OITM T0 INNER JOIN ITM1 T1 ON T0.ItemCode=T1.ItemCode
LEFT JOIN OITW T2 ON T0.ItemCode = T2.ItemCode
WHERE T1.PriceList=4 AND T2.WhsCode='01'
) SUB

GROUP BY SUB.ItmsGrpCod, SUB.ItemCode, SUB.ItemName, SUB.CardCode, SUB.SalFactor2, SUB.CodeBars
ORDER BY SUB.ItemName"

		@articles = Oitm.find_by_sql(req)
	
	end
	
	def export_tableur
	
		if @articles then
			
			chemin = File.join(@window.chemin_temp,"articles.csv")
			CSV.open(chemin, "wb", {:col_sep => ";"}) do |csv|
				csv << ["Article", "CAD", "Code", "Groupe", "Colisage", "Prix comptant", "Prix credit", "Prix achat", "Prix revient", "Stock"]
				@articles.each { |a|
					csv << [a.nom, a.code_barre, a.code, a.groupe, a.colisage, a.prix_comptant, a.prix_credit, a.prix_achat, a.prix_revient, (a.stock ? a.stock : 0.0)]
				}
			end
			
			if RUBY_PLATFORM.include?("linux") then 
				system "xdg-open","#{chemin}"
			else
				if RUBY_PLATFORM.include?("mingw") then 
					system "start","#{chemin}"
				else
		
			end
		end
		
		end
	
	end
	
	def refresh
	
		requete
		remplir
	
	end
	
	def remplir

		@list_store.clear
		
		i=0
		@articles.each { |h| 	
			iter = @list_store.append nil
			iter[0] = h.nom
			iter[1] = h.code_barre
			iter[2] = h.code
			iter[3] = h.groupe.to_s
			iter[4] = "%.2f" % h.colisage
			iter[5] = "%.2f" % h.prix_comptant
			iter[6] = "%.2f" % h.prix_credit
			iter[7] = "%.2f" % h.prix_achat
			iter[8] = "%.2f" % h.prix_revient
			iter[9] = (h.stock ? "%.2f" % h.stock : "0.00")
			i += 1
		}
		
		if i>0
			@pdf.sensitive = true
			@calc.sensitive = true
		end
			
	end
	
	def imprimer resultats
	
		chemin = File.join(@window.chemin_temp,"articles.pdf")
		EditionArticles.new resultats, chemin

		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open","#{chemin}"
		else
			if RUBY_PLATFORM.include?("mingw") then 
				system "start","#{chemin}"
			else
		
			end
		end
		
	end
	
	def focus
		@calcule.grab_focus
	end
	
end
