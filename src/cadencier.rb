# coding: utf-8

class Cadencier < Gtk::Box

	def initialize window
	
		super(:vertical, 2)
		
		@window = window
		
		self.pack_start formulaire, :expand => false, :fill => false, :padding => 2
		self.pack_start tableau, :expand => true, :fill => true, :padding => 2
		
		hbox = Gtk::Box.new :horizontal, 2
		# Bouton PDF
		@pdf = Gtk::Button.new
		hboxvalider = Gtk::Box.new :horizontal, 2
		hboxvalider.add Gtk::Image.new( :file => "./resources/pdf.png" )
		hboxvalider.add Gtk::Label.new "Edition PDF"
		@pdf.add hboxvalider
		@pdf.sensitive = false
		hbox.add @pdf
		@pdf.signal_connect( "clicked" ) { pdf }
		
		# Bouton Calc
		@calc = Gtk::Button.new
		hboxcalc = Gtk::Box.new :horizontal, 2
		hboxcalc.add Gtk::Image.new( :file => "./resources/calc.png" )
		hboxcalc.add Gtk::Label.new "Export Tableur"
		@calc.add hboxcalc
		@calc.sensitive = false
		hbox.add @calc
		@calc.signal_connect( "clicked" ) { export_tableur }
		
		align = Gtk::Alignment.new 1, 0.5, 0, 0
		align.add hbox
		
		self.pack_end align, :expand => false, :fill => false, :padding => 2
	
	end
	
	def formulaire
		
		hbox = Gtk::Box.new :horizontal, 2
		hbox.pack_start Gtk::Label.new("Fournisseur :"), :expand => false, :fill => false, :padding => 2
		@fournisseur = Gtk::Entry.new
		hbox.pack_start @fournisseur, :expand => false, :fill => false, :padding => 2
		hbox.pack_start Gtk::Label.new("Date de départ :"), :expand => false, :fill => false, :padding => 2
		@date = Gtk::Entry.new
		hbox.pack_start @date, :expand => false, :fill => false, :padding => 2
		@calcule = Gtk::Button.new
		hboxcalcule = Gtk::Box.new :horizontal, 2
		hboxcalcule.add Gtk::Image.new( :file => "./resources/validate.png" )
		hboxcalcule.add Gtk::Label.new "Calculer"
		@calcule.add hboxcalcule
		hbox.pack_start @calcule, :expand => false, :fill => false, :padding => 2
		
		@calcule.signal_connect("clicked") {refresh @fournisseur.text, @date.text}
		@fournisseur.signal_connect('activate') {refresh @fournisseur.text, @date.text}
		@date.signal_connect('activate') {refresh @fournisseur.text, @date.text}
		
		return hbox
		
	end
	
	def tableau
	
		@list_store = Gtk::TreeStore.new(String, Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer, String)
		@view = Gtk::TreeView.new(@list_store)
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		col = Gtk::TreeViewColumn.new("Code produit", renderer_left, :text => 10)
		col.sort_column_id = 10
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Désignation produit", renderer_left, :text => 0)
		col.sort_column_id = 0
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Stock actuel", renderer_right, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Sem -1", renderer_right, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Sem -2", renderer_right, :text => 3)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Sem -3", renderer_right, :text => 4)
		col.sort_column_id = 4
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Sem -4", renderer_right, :text => 5)
		col.sort_column_id = 5
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Sem -5", renderer_right, :text => 6)
		col.sort_column_id = 6
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Sem -6", renderer_right, :text => 7)
		col.sort_column_id = 7
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Sem -7", renderer_right, :text => 8)
		col.sort_column_id = 8
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Sem -8", renderer_right, :text => 9)
		col.sort_column_id = 9
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def pdf
		
		header = []
		@view.columns.each do |c|
			header << c.title
		end
		header << "Commentaires"
		
		results = []
		@articles.each_key { |code| 	
			results << [code.to_s, @articles[code][:nom].to_s[0..40], @articles[code][:stock].to_i, @articles[code][:s1].to_i, @articles[code][:s2].to_i, @articles[code][:s3].to_i, @articles[code][:s4].to_i, @articles[code][:s5].to_i, @articles[code][:s6].to_i, @articles[code][:s7].to_i, @articles[code][:s8].to_i, ""]
		}

		if !results.empty?
			imprimer header, results
		else
			@window.message_info "Aucun résultat pour cette sélection."
		end
	
	end
	
	def requete fournisseur, datedeb
	
		@articles = {}
		if fournisseur
			req = "
			SELECT T0.ItemCode as code, T0.ItemName AS nom, T2.Onhand as stock
			FROM OITM T0
			LEFT JOIN OITW T2 ON T0.ItemCode = T2.ItemCode
			WHERE T2.WhsCode='01' AND T0.CardCode='#{fournisseur}' AND T0.SellItem='Y'
			"
			# On sélectionne les données
			articles = Oitm.find_by_sql(req)
			if datedeb.to_s.empty?
				date = Date.today
			else
				begin
					date = Date.parse(datedeb)
				rescue
					date = Date.today
				end
			end
			@date.text = date.strftime("%d/%m/%Y")
			articles.each do |a|
				s1 = s2 = s3 = s4 = s5 = s6 = s7 = s8 = 0
				d = date
				@semaines = []
				(1..8).to_a.each do |i|
					d = d-6-d.cwday
					datedeb = d.strftime("%Y%m%d")
					datefin = (d+6).strftime("%Y%m%d")
					@semaines << "Sem #{d.cweek}"
					req = "
					SELECT T0.ItemCode AS code, SUM(T0.quantity) AS quantity
					FROM PDN1 T0
					INNER JOIN OPDN T1 ON T0.DocEntry=T1.DocEntry
					WHERE T0.ItemCode='#{a.code}' 
					AND T1.CardCode='#{fournisseur}'
					AND T1.DocDate BETWEEN '#{datedeb}' AND '#{datefin}'
					GROUP BY T0.ItemCode
					"
					commandes = Opdn.find_by_sql(req)
					
					unless commandes.empty?
						case i
							when 1
								s1 = commandes.first.quantity
							when 2
								s2 = commandes.first.quantity
							when 3
								s3 = commandes.first.quantity
							when 4
								s4 = commandes.first.quantity
							when 5
								s5 = commandes.first.quantity
							when 6
								s6 = commandes.first.quantity
							when 7
								s7 = commandes.first.quantity
							when 8
								s8 = commandes.first.quantity
						end
					end
				end
				@articles.merge!({a.code => {:nom => a.nom, :stock => a.stock, :s1 => s1, :s2 => s2, :s3 => s3, :s4 => s4, :s5 => s5, :s6 => s6, :s7 => s7, :s8 => s8}})
			end
		end
	
	end
	
	def export_tableur
	
		if @articles then
			
			chemin = File.join(@window.chemin_temp,"articles.csv")
			CSV.open(chemin, "wb", {:col_sep => ";"}) do |csv|
				csv << ["Code produit", "Designation produit", "Stock actuel"] + @semaines
				@articles.each_key { |code| 
					csv << [code.to_s, @articles[code][:nom].to_s, @articles[code][:stock].to_i, @articles[code][:s1].to_i, @articles[code][:s2].to_i, @articles[code][:s3].to_i, @articles[code][:s4].to_i, @articles[code][:s5].to_i, @articles[code][:s6].to_i, @articles[code][:s7].to_i, @articles[code][:s8].to_i]
				}
			end
			
			if RUBY_PLATFORM.include?("linux") then 
				system "xdg-open","#{chemin}"
			else
				if RUBY_PLATFORM.include?("mingw") then 
					system 'start "" "' + chemin + '"'
				else
		
			end
			
		end
		
		end
	
	end
	
	def refresh fournisseur=nil, datedeb=nil
	
		requete(fournisseur, datedeb)
		remplir
	
	end
	
	def remplir

		@list_store.clear
		
		i=0
		@articles.each_key { |code| 	
			iter = @list_store.append nil
			iter[0] = @articles[code][:nom].to_s
			iter[1] = @articles[code][:stock].to_i
			iter[2] = @articles[code][:s1].to_i
			iter[3] = @articles[code][:s2].to_i
			iter[4] = @articles[code][:s3].to_i
			iter[5] = @articles[code][:s4].to_i
			iter[6] = @articles[code][:s5].to_i
			iter[7] = @articles[code][:s6].to_i
			iter[8] = @articles[code][:s7].to_i
			iter[9] = @articles[code][:s8].to_i
			iter[10] = code.to_s
			i += 1
		}

		(0..7).to_a.each do |i|
			@view.columns[i+3].title = @semaines[i] if @semaines
		end
		
		@pdf.sensitive = i>0
		@calc.sensitive = i>0
		
	end
	
	def imprimer header, results
	
		chemin = File.join(@window.chemin_temp,"articles.pdf")
		titre = "SIS - Cadencier d'achat - Fournisseur #{@fournisseur.text}"
		EditionCadencier.new header, results, chemin, titre

		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open","#{chemin}"
		else
			if RUBY_PLATFORM.include?("mingw") then 
				system 'start "" "' + chemin + '"'
			else
		
			end
		end
		
	end
	
	def focus
		@fournisseur.grab_focus
	end
	
end
