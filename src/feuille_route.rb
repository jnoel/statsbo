# coding: utf-8

class FeuilleRoute < Gtk::Box

	def initialize window
	
		super(:vertical, 2)
		
		@window = window
		
		self.pack_start formulaire, :expand => false, :fill => false, :padding => 2
		self.pack_start tableau, :expand => true, :fill => true, :padding => 2
		
		hbox = Gtk::Box.new :horizontal, 2
		# Bouton PDF
		@pdf = Gtk::Button.new
		hboxvalider = Gtk::Box.new :horizontal, 2
		hboxvalider.add Gtk::Image.new( :file => "./resources/pdf.png" )
		hboxvalider.add Gtk::Label.new "Edition PDF"
		@pdf.add hboxvalider
		@pdf.sensitive = false
		hbox.add @pdf
		@pdf.signal_connect( "clicked" ) { pdf }
		
		# Bouton Calc
		@calc = Gtk::Button.new
		hboxcalc = Gtk::Box.new :horizontal, 2
		hboxcalc.add Gtk::Image.new( :file => "./resources/calc.png" )
		hboxcalc.add Gtk::Label.new "Export Tableur"
		@calc.add hboxcalc
		@calc.sensitive = false
		hbox.add @calc
		@calc.signal_connect( "clicked" ) { export_tableur }
		
		align = Gtk::Alignment.new 1, 0.5, 0, 0
		align.add hbox
		
		self.pack_end align, :expand => false, :fill => false, :padding => 2
	
	end
	
	def formulaire
		
		hbox = Gtk::Box.new :horizontal, 2
		#hbox.pack_start Gtk::Label.new("Transporteur :"), false, false, 2
		@chauffeur = Gtk::ComboBoxText.new 
		#remplir_chauffeur
		#hbox.pack_start @chauffeur, false, false, 2
		hbox.pack_start Gtk::Label.new("Date de factures :"), :expand => false, :fill => false, :padding => 2
		@date = Gtk::Entry.new
		hbox.pack_start @date, :expand => false, :fill => false, :padding => 2
		@groupe = Gtk::ComboBoxText.new 
		@groupe.append_text "Frais & Surgelés"
		@groupe.active = 0
		hbox.pack_start Gtk::Label.new("Groupe d'articles"), :expand => false, :fill => false, :padding => 2
		hbox.pack_start @groupe, :expand => false, :fill => false, :padding => 2
		@calcule = Gtk::Button.new
		hboxcalcule = Gtk::Box.new :horizontal, 2
		hboxcalcule.add Gtk::Image.new( :file => "./resources/validate.png" )
		hboxcalcule.add Gtk::Label.new "Calculer"
		@calcule.add hboxcalcule
		hbox.pack_start @calcule, :expand => false, :fill => false, :padding => 2
		
		@calcule.signal_connect("clicked") {refresh @chauffeur.active_text, @groupe.active_text, @date.text}
		@date.signal_connect('activate') {refresh @chauffeur.active_text, @groupe.active_text, @date.text}
		
		return hbox
		
	end
	
	def tableau number=0
	
		liste = [String, Integer]
		number.times do
			liste << String
			liste << Integer
		end
		
		list_store = Gtk::TreeStore.new(*liste)
		@view = Gtk::TreeView.new(list_store)
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0.5
		
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = "white"
		renderer_right.xalign = 1
		renderer_right.yalign = 0.5
		
		col = Gtk::TreeViewColumn.new("Article", renderer_left, :text => 0)
		col.sort_column_id = 0
		col.resizable = true
		col.expand = true
		@view.append_column(col)

		(1..number).to_a.each do |i|
			col = Gtk::TreeViewColumn.new("", renderer_right, :text => i*2)
			col.sort_column_id = i*2+1
			col.resizable = true
			@view.append_column(col)
		end
		
		col = Gtk::TreeViewColumn.new("Total", renderer_right, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	scroll.add @view

  	scroll
	
	end
	
	def add_columns number
	
		self.remove self.children[1]
		self.pack_start tableau(number), :expand => true, :fill => true, :padding => 2
		self.show_all
	
	end
	
	def pdf
		
		result = []

		result << ["Article"] + @columns + ["Total"]
		@view.model.each { |model, path, iter|
			tab = [iter[0]]
			@columns.each_index { |i|
				tab << iter[i*2+2]
			}
			tab << iter[1]
			result << tab
		}

		if result.count>1
			imprimer result
		else
			@window.message_info "Aucun résultat pour cette sélection."
		end
	
	end
	
	def requete chaufeur, groupe, datedeb
	
		if datedeb.to_s.empty?
			date = Date.today
		else
			begin
				date = Date.parse(datedeb)
			rescue
				date = Date.today
			end
		end
		date = Date.today if (date.year<2000 or date.year>3000)
		@date.text = date.strftime("%d/%m/%Y")
		datedeb = date.strftime("%Y%m%d")
		
		@articles = {}
		@bls = {}
		
		if (@groupe.active>-1)
			req = "
			SELECT T0.ItemCode as code, T0.Dscription AS designation, T0.factor1 AS nb_colis, T0.factor2 AS colisage, T0.quantity AS nb_total, T1.DocNum AS ref, T3.CardCode AS client_code, T3.CardName AS client_nom
			FROM INV1 T0
			LEFT JOIN OINV T1 ON T0.DocEntry = T1.DocEntry
			LEFT JOIN OITM T2 ON T0.ItemCode = T2.ItemCode
			LEFT JOIN OCRD T3 ON T1.CardCode = T3.CardCode
			WHERE T2.ItmsGrpCod=102 AND T1.DocDate='#{datedeb}' AND T1.DocStatus!='C' AND T1.U_BPFS='OUI'
			"
			
			# On sélectionne les données
			articles = Oitm.find_by_sql(req)
			articles.each do |a|
				if @articles[a.code]
					if @articles[a.code][:BLS][a.ref]
						temp = @articles[a.code][:BLS][a.ref][:ligne]
						@articles[a.code][:BLS][a.ref][:ligne] = {:nb_colis => a.nb_colis+temp[:nb_colis],
																											:colisage => a.colisage.to_i,
																				 						  :total => a.nb_total.to_i+temp[:total]
																										 }
						@articles[a.code][:total] = @articles[a.code][:total].to_i+a.nb_total.to_i
					else
						@articles[a.code][:BLS][a.ref] =  {:code_client => a.client_code,
																							 :nom_client => a.client_nom,
																							 :ligne => {:nb_colis => a.nb_colis.to_i,
																						   						:colisage => a.colisage.to_i,
																						 						  :total => a.nb_total.to_i
																							 					 }
																						 }
						@articles[a.code][:total] = @articles[a.code][:total].to_i+a.nb_total.to_i
					end
				else
					@articles[a.code] = {:designation => a.designation,
															 :BLS => {a.ref => {:code_client => a.client_code,
																							   :nom_client => a.client_nom,
																							   :ligne => {:nb_colis => a.nb_colis.to_i,
																							   						:colisage => a.colisage.to_i,
																							 						  :total => a.nb_total.to_i
																							 					 	 }
																							  }
																		  },
															 :total => a.nb_total.to_i
														  }
				end
				unless @bls[a.ref]
					bl = {:code_client => a.client_code,
								:nom_client => a.client_nom
							 }
					@bls[a.ref] = bl
				end
			end
			
		end
	end
	
	def export_tableur
	
		if @articles then
			
			chemin = File.join(@window.chemin_temp,"BPFS.csv")
			CSV.open(chemin, "wb", {:col_sep => ";"}) do |csv|
				csv << ["Article"] + @columns + ["Total"]
				@view.model.each { |model, path, iter|
					tab = [iter[0]]
					@columns.each_index { |i|
						tab << iter[i*2+2]
					}
					tab << iter[1]
					csv << tab
				}
			end
			
			if RUBY_PLATFORM.include?("linux") then 
				system "xdg-open","#{chemin}"
			else
				if RUBY_PLATFORM.include?("mingw") then 
					system 'start "" "' + chemin + '"'
				else
		
			end
			
		end
		
		end
	
	end
	
	def refresh chauffeur=nil, groupe=nil, datedeb=nil
	
		requete(chauffeur, groupe, datedeb)
		remplir
	
	end
	
	def remplir

		@view.model.clear
		
		i=0
		@bls.each_key { |code|
			i += 1
		}
		add_columns i
		
		i=1
		@columns = []
		@bls.each_key { |code|
			@view.columns[i].title = code.to_s
			@bls[code][:position] = i
			i += 1
			@columns[i] = "#{code} - #{@bls[code][:nom_client]}"
		}
		@columns.delete nil
		
		@articles.each_key { |code| 	
			iter = @view.model.append nil
			iter[0] = "#{code} - #{@articles[code][:designation]}"
			@articles[code][:BLS].each_key { |bl|
				pos = @bls[bl][:position]
				ligne = @articles[code][:BLS][bl][:ligne]
				iter[pos*2] = "#{ligne[:total]/ligne[:colisage]}x#{ligne[:colisage]}=#{ligne[:total]}"
				iter[pos*2+1] = ligne[:total]
			}
			iter[1] = @articles[code][:total]
		}
		
		@pdf.sensitive = i>1
		@calc.sensitive = i>1
		
	end
	
	def remplir_chauffeur
		Ufd1.where(:FieldID => 8).each { |t|
			@chauffeur.append_text t.Descr
		}
	end
	
	def imprimer resultats
	
		chemin = File.join(@window.chemin_temp,"feuille_route.pdf")
		titre = "SIS - Feuille de route - Frais & Surgelés - #{@date.text}"
		EditionFeuilleRoute.new resultats, chemin, titre

		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open","#{chemin}"
		else
			if RUBY_PLATFORM.include?("mingw") then 
				system 'start "" "' + chemin + '"'
			else
		
			end
		end
		
	end
	
	def focus
		@date.grab_focus
	end
	
end
