# coding: utf-8

class EditionCadencier < Prawn::Document
	
	attr_reader :fichier
	
	def initialize header, results, chemin, titre
	
		super(	:page_layout => :landscape,
						:page_size => 'A4'
				 )
				 
		en_tete titre
		corps header, results
		rendu chemin

	end
	
	def en_tete titre
		
		bounding_box([0,cursor], :width => bounds.right) do
  		text titre, :size => 12
		end
		move_down 20
	
	end
	
	def corps header, results
		  		 		
		bounding_box([0,cursor], :width => bounds.right) do
  		t = table(results.insert(0,header)) do |table|
  			table.header = true
  			table.cell_style = { :size => 8, :align => :right}
  			table.row(0).style :align => :center
  			table.column(0).style :align => :left
  			table.column(1).style :align => :left
  			table.column(11).width = 150
			end
		end
	
	end
	
	def rendu chemin

		render_file chemin
	
	end
	
end
