# coding: utf-8

class EditionArticles < Prawn::Document
	
	attr_reader :fichier
	
	def initialize resultats, chemin
	
		super(	:page_layout => :landscape,
						:page_size => 'A4'
				 )
				 
		en_tete
		corps resultats
		rendu chemin

	end
	
	def en_tete
		
		bounding_box([0,cursor], :width => bounds.right) do
  		text "SIS - Liste d'articles", :size => 12
		end
		move_down 20
	
	end
	
	def corps resultats
		
		en_tete = ["Article", "CAD", "N°Art", "Groupe", "Colisage", "Prix cptt", "Prix crdt", "Prix HA", "Prix revient", "Stock"]
  		 		
		bounding_box([0,cursor], :width => bounds.right) do
  		t = table(resultats.insert(0,en_tete)) do |table|
  			table.header = true
  			table.cell_style = { :size => 8, :align => :right}
  			table.row(0).style :align => :center
  			table.column(0).style :align => :left
			end
		end
	
	end
	
	def rendu chemin

		render_file chemin
	
	end
	
end
