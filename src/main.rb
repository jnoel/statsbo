# coding: utf-8

class MainWindow < Gtk::Window
	
	attr_reader :stat_commercial, :stat_articles, :version, :chemin_temp, :cadencier, :feuille_route
	
	def initialize level
	
		super
		
		signal_connect( "destroy" ) { Gtk.main_quit }
		
		@version = "3.0"
		set_title GLib.application_name + " v" + @version
		
		set_window_position :center
		set_default_size 850, 300
		maximize
		set_icon( "./resources/icon.png" )
		
		@chemin_temp = File.join(Dir.home,".statsbo")
		Dir.mkdir(@chemin_temp) unless Dir.exist?(@chemin_temp)
		
		lancement
		active_menu
	
	end
	
	def lancement
		
		@toolbargen = ToolBarGen_box.new self
		
		@vbox = Gtk::Box.new :vertical, 2
		@vbox.pack_start @toolbargen, :expand => false, :fill => false, :padding => 0

		@stat_commercial = StatCommercial.new self
		@stat_articles = StatArticles.new self
		@cadencier = Cadencier.new self
		@feuille_route = FeuilleRoute.new self
		
		@vbox.pack_start Gtk::Image.new( :file => "./resources/icon.png" ), :expand => true, :fill => true, :padding => 0
		
		add @vbox
		
		show_all
		
		@stat_commercial.focus
	
	end
	
	def active_menu
	
		#@toolbargen.each { |child|
		#	child.sensitive = true
		#}
		
		# If not a admin, config menu is hidden
		#@toolbargen.children[11].sensitive = false unless @login.user.admin
		
		set_title "#{GLib.application_name} v #{@version}"
	
	end
	
	def efface_vbox_actuelle
	
		@vbox.remove @vbox.children[@vbox.children.count-1] if @vbox.children.count.eql?(2)
	
	end
	
	def affiche vbox
	
		efface_vbox_actuelle
		@vbox.pack_start vbox, :expand => true, :fill => true, :padding => 0
        self.show_all
	
	end	
	
	def quit
		
		dialog = Gtk::MessageDialog.new(:parent => self, 
                                		:flags => :destroy_with_parent,
                                		:type => :question,
                                		:buttons_type => :yes_no,
                                		:message => "Voulez-vous réellement quitter #{GLib.application_name} ?")
		response = dialog.run
		case response
		  when Gtk::ResponseType::YES
				Gtk.main_quit
		end 
		dialog.destroy
		
	end
	
	def message_erreur message
		p message
		m = "Impossible d'exécuter la requête demandée...\nVoici le message d'erreur affiché :\n" + message
		dialog = Gtk::MessageDialog.new(:parent => self, 
                                		:flags => :destroy_with_parent,
                                		:type => :error,
                                		:buttons_type => :ok,
                                		:message => m)
		response = dialog.run
		dialog.destroy
	end
	
	def message_info message
		p message
		dialog = Gtk::MessageDialog.new(:parent => self, 
                                		:flags => :destroy_with_parent,
                                		:type => :info,
                                		:buttons_type => :ok,
                                		:message => message)
		response = dialog.run
		dialog.destroy
	end

end
