# coding: utf-8

class StatCommercial < Gtk::Box

	def initialize window
	
		super(:vertical, 2)
		
		@window = window
		
		@resultats = nil
		
		self.pack_start formulaire, :expand => false, :fill => false, :padding => 2
		self.pack_start tableau, :expand => true, :fill => true, :padding => 2
		
		hbox = Gtk::Box.new :horizontal, 2
		# Bouton PDF
		@pdf = Gtk::Button.new
		hboxvalider = Gtk::Box.new :horizontal, 2
		hboxvalider.add Gtk::Image.new( :file => "./resources/pdf.png" )
		hboxvalider.add Gtk::Label.new "Edition PDF"
		@pdf.add hboxvalider
		@pdf.sensitive = true
		hbox.add @pdf
		@pdf.signal_connect( "clicked" ) {  
			@resultats = requete(@decoupage.active==0)
			imprimer @resultats, @decoupage.active==0 if @resultats 
		}
		
		# Bouton Calc
		@calc = Gtk::Button.new
		hboxcalc = Gtk::Box.new :horizontal, 2
		hboxcalc.add Gtk::Image.new( :file => "./resources/calc.png" )
		hboxcalc.add Gtk::Label.new "Export Tableur"
		@calc.add hboxcalc
		@calc.sensitive = true
		hbox.add @calc
		@calc.signal_connect( "clicked" ) { 
			@resultats = requete(@decoupage.active==0)
			export_tableur @resultats if @resultats
		}
		
		align = Gtk::Alignment.new 1, 0.5, 0, 0
		align.add hbox
		
		self.pack_end align, :expand => false, :fill => false, :padding => 2
	
	end
	
	def formulaire
		
		hbox = Gtk::Box.new :horizontal, 2
		
		hbox.pack_start Gtk::Label.new("Commercial :"), :expand => false, :fill => false, :padding => 2
		
		@commercial = Gtk::ComboBoxText.new
		remplir_commerciaux
		hbox.pack_start @commercial, :expand => false, :fill => false, :padding => 2
		
		@datedeb = Gtk::Entry.new
		@datefin = Gtk::Entry.new
		hbox.pack_start Gtk::Label.new("Date de début :"), :expand => false, :fill => false, :padding => 2
		hbox.pack_start @datedeb, :expand => false, :fill => false, :padding => 2
		hbox.pack_start Gtk::Label.new("Date de fin :"), :expand => false, :fill => false, :padding => 2
		hbox.pack_start @datefin, :expand => false, :fill => false, :padding => 2
		
		@decoupage = Gtk::ComboBoxText.new
		@decoupage.append_text "par mois"
		@decoupage.append_text "par semaine"
		hbox.pack_start Gtk::Label.new("Découpage :"), :expand => false, :fill => false, :padding => 2
		hbox.pack_start @decoupage, :expand => false, :fill => false, :padding => 2
		@decoupage.active=0
		
		@calcule = Gtk::Button.new
		hboxcalcule = Gtk::Box.new :horizontal, 2
		hboxcalcule.add Gtk::Image.new( :file => "./resources/validate.png" )
		hboxcalcule.add Gtk::Label.new "Calculer"
		@calcule.add hboxcalcule
		#hbox.pack_start @calcule, :expand => false, :fill => false, :padding => 2
		
		# Bouton PDF
		@pdf = Gtk::Button.new
		hboxvalider = Gtk::Box.new :horizontal, 2
		hboxvalider.add Gtk::Image.new( :file => "./resources/pdf.png" )
		hboxvalider.add Gtk::Label.new "Edition PDF"
		@pdf.add hboxvalider
		@pdf.sensitive = true
		hbox.pack_start @pdf, :expand => false, :fill => false, :padding => 2
		@pdf.signal_connect( "clicked" ) {  
			@resultats = requete(@decoupage.active==0)
			imprimer @resultats, @decoupage.active==0 if @resultats 
		}
		
		# Bouton Calc
		@calc = Gtk::Button.new
		hboxcalc = Gtk::Box.new :horizontal, 2
		hboxcalc.add Gtk::Image.new( :file => "./resources/calc.png" )
		hboxcalc.add Gtk::Label.new "Export Tableur"
		@calc.add hboxcalc
		@calc.sensitive = true
		hbox.pack_start @calc, :expand => false, :fill => false, :padding => 2
		@calc.signal_connect( "clicked" ) { 
			@resultats = requete(@decoupage.active==0)
			export_tableur @resultats if @resultats
		}
		
		@calcule.signal_connect("clicked") {}
		
		return hbox
		
	end
	
	def remplir_commerciaux
	
		comm = Oslp.order(:slpname)
		comm.each do |c|
			@commercial.append_text c.SlpName
		end
		@commercial.active = 0
	
	end
	
	def tableau
	
		box = Gtk::Box.new :vertical, 2
		box
	
	end
	
	def requete mois_bool=true
	
		# On récupère les dates de début et de fin
		datedeb = Date.parse(@datedeb.text)
		datefin = Date.parse(@datefin.text)
		# On sélectionne les données
		comm = Oslp.where(:slpname => @commercial.active_text).first
		bls = Oinv.where(:docduedate => datedeb.strftime("%Y%m%d")..datefin.strftime("%Y%m%d"), :slpcode => comm.SlpCode).order(:cardcode)
		
		# Préparer un tableau de CA et un de total vide avec le bon nombre de mois
		tab_ca = []
		total = []
		if mois_bool
			(datedeb.month..datefin.month).to_a.each do |m|
				tab_ca << 0.0
				total << 0.0
			end
		else
			(datedeb.cweek..datefin.cweek).to_a.each do |m|
				tab_ca << 0.0
				total << 0.0
			end
		end
		
		# Si il y a des BLs, on déclare les variables et on commence le parcours de la liste	
		if bls.count>0
			resultats = []
			res = nil
			cardcode = "0"
			bls.each do |bl|
				# Si nouveau client, on stocke les données de l'ancien dans resultats et on commence un nouveau
				if cardcode!=bl.CardCode
					cardcode = bl.CardCode
					resultats << res unless res.nil?
					res = {:client => "#{bl.CardCode} - #{bl.CardName}",
								 :ca => tab_ca+[]
								}
				end
				# connaitre la position du mois en cours dans le tableau des CA
				i = nil
				if mois_bool
					i = bl.DocDueDate.month-datedeb.month-1
				else
					i = bl.DocDueDate.strftime("%W").to_i-datedeb.cweek
				end
				# et on l'ajoute au total du ca pour le mois pour le client en cours
				res[:ca][i] += bl.DocTotal
				res[:ca][i] = res[:ca][i].round(2)	
				
			end
			# on enregistre le dernier client de la liste dans resultats
			resultats << res unless res.nil?
			
			# On totalise chaque colonne
			(0..total.count-1).to_a.each do |i|
				resultats.each do |r|
					total[i] += r[:ca][i]
				end
				total[i] = total[i].round(2)
			end
			total = total.insert(0, "Total")
			
			mois_libelle = ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Décembre"]
		
			periode = [""]
			clients = []
		
			if mois_bool
				(datedeb.month..datefin.month).to_a.each do |i|
					periode << mois_libelle[i-1]
				end
			else
				(datedeb.cweek..datefin.cweek).to_a.each do |i|
					periode << i
				end
			end
		
			resultats.each do |res|
				clients << res[:ca].insert(0,res[:client])
			end
			
			clients << total
		
			return clients.insert(0,periode)
		else
			@window.message_info "Aucun résultat pour cette sélection."
			return false
		end
		
	end
	
	def export_tableur resultats
			
		chemin = File.join(@window.chemin_temp,"stat_commercial.csv")
		CSV.open(chemin, "wb", {:col_sep => ";"}) do |csv|
			resultats.each { |r|
				csv << r
			}
		end
		
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open","#{chemin}"
		else
			if RUBY_PLATFORM.include?("mingw") then 
				system "start","#{chemin}"
			else
			
			end
		end
	
	end
	
	def focus
		@commercial.grab_focus
	end
	
	def imprimer resultats, mois
		
		chemin = File.join(@window.chemin_temp,"ca.pdf")
		datedeb = Date.parse(@datedeb.text)
		datefin = Date.parse(@datefin.text)
		Edition.new resultats, mois, chemin, datedeb, datefin, @commercial.active_text

		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open","#{chemin}"
		else
			if RUBY_PLATFORM.include?("mingw") then 
				system "start","#{chemin}"
			else
		
			end
		end
		
	end
	
end
